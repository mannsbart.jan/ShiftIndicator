# Deprecated!

Please use [this](https://github.com/janmannsbart23/OBDShiftIndicator) repo instead.

# ShiftIndicator

![Screenshot](Images/ShiftIndicatorFront.jpg)
ShiftIndicator is 

  - an indicator to upshift one gear in your car (widely know in race cars)
  - developed as a hobby project 

Videos:

[![Alt text](https://img.youtube.com/vi/AC_jxDbr7xE/0.jpg)](https://www.youtube.com/watch?v=AC_jxDbr7xE)

# Complete Project!

  - The Project contains anything you need to build a working shiftindicator on your own including a pcb, a housing and code for running it.
  - It was developed over a long period as a hobby and was constantly improving
  

# Three Main Components

The ShiftIndicator consists of three main components

* PCB for mainboard and shitindicator-board
* 3D printed housing (designed for a mini cooper (easy to be adapted))
* Code for microcontroller

# PCB Parts

The pcb were designed in eagle. These files are available in the repository. Beside them the gerber files ready for production are also provided and can be uploaded to your favourite pcb manufacturer. 

There are two pcbs, one for the shiftindicator itself and one who holds the esp32, named "mainboard". The two pcbs are conencted through a micro usb cable.

For assembling the pcbs you need a few smd components. These are:

##### ShiftIndicator pcb partlist:

List of materials for the shiftindicator pcb

| Part | Value | Package |
| ------ | ------ | --- |
| LED1 |  WS2812B    |                                                             
| LED2|  WS2812B   |                                      
| LED3|  WS2812B  |                                                          
| LED4|  WS2812B   |                                                
| LED5|  WS2812B  |                                                
| LED6|  WS2812B  |                                                 
| LED7|  WS2812B  |                                      
| LED8|  WS2812B  |                                      
| LED9|  WS2812B  |    
| C1    | 100 nF    | 0805  |
| R1|    100|      0805| 
| R2  |  47    |   0805| 
| S1   |       |   TACTILE_SWITCH_SMD_5.2MM      | 
| USB    |      |  Micro USB vertical            |                                
| Photoresitor    | 5528    |   |

##### Mainboard pcb partlist:
 
 List of materials for the mainboard pcb
  
| Part | Value | Package |
| ------ | ------ | --- |
|C1 |22µF| 0805|
|ESP32   |  | ESP32DEVKIT |                         
|OBD_SERIAL |  |Pin Header 2,54mm     |                         
| USB | |USB Type A vertical  |   

# 3D Housing

The 3D housing was designed in solid edge. Besides the solid edge files the .stl files are provided. They can straight up be printed. You need the housing itself and the backplate.
To secure the backplate to the housiing you will need two M2 screws and two M2 inner thread to stick in the 3d printed housing.

# Code

The code was developed using the arduino core for the esp32. For multithreading FreeRTOS was used. You will need the FastLED library to compile the project. Also you will need an ELM327 OBD device with an uart interface. I used one from freematics, but there are also other possibilities.
The code is widely commented, so i hope a lot of upcoming questions can be clarified by them.

![Screenshot](Code/ComponentOverview.jpg)

Short feature list:
* Button for switching between differend lightmodes
* Brightness sensor to dim the leds according to the daylight
* Bootcounter
* Ability to be controled via bluetooth (Android app is in development)
* Usage of eeprom
* Usage of data queues
* Usage of tasks
* Usage of semaphores



# Contribution
I would be very happy if there are some interested people who like this project and maybe even try it yourself. Since the system itself was developed for a mini cooper, there are certainly things that should be adapted if it should be used in other vehicles (rpm, 3d housing, etc.). For questions, I am always ready.
