/*
MIT License

Copyright (c) 2018 Jan Mannsbart

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#pragma message "Check or fix OBD Protocol if communcation with OBD wont work!"

#include <Preferences.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "FastLED.h"
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <WiFi.h>

#define VERSION "ShiftIndicator V1.2"

//FEATURES
//#define BLUETOOTH
//#define WIFI
#define MODE_SWITCH_BUTTON
#define SERIAL_DEBUG  //Needs to be enabled for any debug output on the serial console

//DEBUG
//#define SYSTEM_DEBUG
//#define LED_DEBUG
//#define BRIGHTNESS_DEBUG
//#define OBD_DEBUG
//#define BLUETOOTH_DEBUG
//#define WIFI_DEBUG
//TEST
//#define LED_TEST	//LED's will light in different collors

//Thread priorities
#define HIGH_PRIO 16
#define NORMAL_PRIO 8
#define LOW_PRIO 4

//Light sensor settings
#define LIGHTSENSOR_PIN 33
//Depending on your choosen photo resistor value the values have to be adapted
#define MIN_LED_BRIGHTNESS 4
#define MAX_LED_BRIGHTNESS 64
//Filtering heavy lightsdifferences. Prevents the lights from flickering
#define DIM_TIME_MILLIS 1000

//LED settings
#define LED_PIN 22
#define NUM_LEDS 9
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB

CRGB leds[NUM_LEDS];
CRGB INIT_COLOR = CRGB(15, 15, 255);

//OBD settings
#define OBD_TIMEOUT_MILLIS 1500

//Checkphrases for obd communication
//OK
const char *CHECK_OK = "OK";
const char *CHECK_OK_OBD = "OBDUART";
const char *CHECK_OK_RPMPREFIX = "410C";
//ERROR
const char *CHECK_ERROR_CONNECTION = "UNABLE TO CONNECT";
const char *CHECK_ERROR_BUSINIT = "BUSINIT";
const char *CHECK_ERROR_ENGINEOUT = "410C0000";
//AT commands for communication with elm327
const char *AT_RESET = "ATZ\r";
const char *AT_ECHO_OFF = "ATE0\r";
const char *AT_LINEFEED_ON = "ATL1\r";
const char *AT_SPACES_OFF = "ATS0\r";
/*
0	Automatic protocol detection
1	SAE J1850 PWM (41.6 kbaud)
2	SAE J1850 VPW (10.4 kbaud)
3	ISO 9141-2 (5 baud init, 10.4 kbaud)
4	ISO 14230-4 KWP (5 baud init, 10.4 kbaud)
5	ISO 14230-4 KWP (fast init, 10.4 kbaud)
6	ISO 15765-4 CAN (11 bit ID, 500 kbaud)
7	ISO 15765-4 CAN (29 bit ID, 500 kbaud)
8	ISO 15765-4 CAN (11 bit ID, 250 kbaud) - used mainly on utility vehicles and Volvo
9	ISO 15765-4 CAN (29 bit ID, 250 kbaud) - used mainly on utility vehicles and Volvo
*/
const char *AT_PROTOCOL_AUTO = "ATSP0\r";
const char *AT_PROTOCOL_ISO9141_2 = "ATSP3\r";
const char *CMD_RPM_REQUEST = "010C1\r";

//ESP32 preference setup - used to store boot counter and light mode
Preferences preferences;
static unsigned int bootCounter = 0;
static int shiftLightMode = 0;

//BLE parameters
#define SERVICE_UUID BLEUUID((uint16_t)0x1819)
#define DESCRIPTOR_UUID BLEUUID((uint16_t)0x2901)
#define BOOT_CNT_CHARACTERISTIC_UUID BLEUUID((uint16_t)0xAAA0)
#define LIGHT_MODE_CHARACTERISTIC_UUID BLEUUID((uint16_t)0xAAA1)
#define CMD_LIGHT_MODE_CHARACTERISTIC_UUID BLEUUID((uint16_t)0xAAA2)
#define CONSOLE_CHARACTERISTIC_UUID BLEUUID((uint16_t)0xAAA3)

HardwareSerial DebugSerial(0); // USB Serial
HardwareSerial ObdSerial(2);   // OBD Serial

//Handling interthread communication
xQueueHandle obdDataQueue, lightModeChangeQueue, wifiDebugQueue;
SemaphoreHandle_t colorChange = NULL;
SemaphoreHandle_t shiftLighModeChange = NULL;

/********************************************************************************************************/
/********************************************************************************************************/
///OBD Thread and Functions

void obdThread(void *pvParameter)
{
#ifdef SYSTEM_DEBUG
  LOG_MESSAGE("SYSTEM", "OBD Thread started on core", xPortGetCoreID());
#endif

#ifdef LED_TEST //Show bootanimation. No RPM obd communication is held
  LOG_MESSAGE("LED_TEST","Starting led test");
  while (true)
  {
    showSuccessInit();
    vTaskDelay(2 / portTICK_PERIOD_MS);
  }
#endif
  //Initialize obd until it succeeded
  while (!obdInit())
  {
  }

  //Short led animation
  showSuccessInit();

  leds[4] = CRGB::Black;
  xSemaphoreTake(colorChange, portMAX_DELAY);
  FastLED.show();
  xSemaphoreGive(colorChange);

  //start requesting rpm from obd
  while (true)
  {
    if (!sendMessageWithRetries(CMD_RPM_REQUEST, 4))
    {
      delay(1000);
      obdInit();
    }
  }
}

/*
  Initialize OBD
*/
bool obdInit()
{

#ifdef OBD_DEBUG
  LOG_MESSAGE("OBD", "Starting OBD init");
#endif
  //Signalize that init is started
  leds[0] = INIT_COLOR;
  leds[1] = INIT_COLOR;
  leds[2] = INIT_COLOR;
  leds[3] = INIT_COLOR;
  leds[4] = INIT_COLOR;
  leds[5] = INIT_COLOR;
  leds[6] = INIT_COLOR;
  leds[7] = INIT_COLOR;
  leds[8] = INIT_COLOR;
  xSemaphoreTake(colorChange, portMAX_DELAY);
  FastLED.show();
  xSemaphoreGive(colorChange);

  //Run at commands fpr initialization
  if (!sendMessageWithRetries(AT_RESET, 10))
  {
    return false;
  }
  if (!sendMessageWithRetries(AT_ECHO_OFF, 10))
  {
    return false;
  }
  if (!sendMessageWithRetries(AT_LINEFEED_ON, 10))
  {
    return false;
  }
  if (!sendMessageWithRetries(AT_SPACES_OFF, 10))
  {
    return false;
  }
  //Set OBD protocol (  ISO 9141-2 )
  //Remove or adapt referring to documentation
  if (!sendMessageWithRetries(AT_PROTOCOL_ISO9141_2, 10))
  {
    return false;
  }
  return true;
}

/*
  Short led animation of current led light mode
*/
void showSuccessInit()
{
  for (int i = 0; i < 12; i++)
  {
    buildLeds(i);
    delay(75);
  }
  for (int i = 12; i >= 0; i--)
  {
    buildLeds(i);
    delay(75);
  }
}

/*
  Send an serial command with retries. if no expected response is sent, it will abort
*/
bool sendMessageWithRetries(const char *cmd, int retries)
{
  for (int i = 0; i < retries; i++)
  {
    if (sendCommand(cmd))
    {
      return true;
    }
  }
  return false;
}

/*
  Send an serial command.
*/
bool sendCommand(const char *cmd)
{
  unsigned long startTime = millis();

  ObdSerial.write(cmd);

  while (true)
  {
    delay(5);
    if (ObdSerial.available() > 0)
    {
      String content = "";
      content = ObdSerial.readStringUntil('\>'); // could/must be improved to be failsafe

#ifdef OBD_DEBUG
	  LOG_MESSAGE("OBD", "OBD Response:",content.c_str());
#endif
      if (strstr(content.c_str(), CHECK_OK) != NULL)
      {
#ifdef OBD_DEBUG
		LOG_MESSAGE("OBD","OK",(millis()- startTime),cmd);
#endif
        return true;
      }
      //Connection Error
      else if (strstr(content.c_str(), CHECK_ERROR_CONNECTION) != NULL)
      {
#ifdef OBD_DEBUG
	  LOG_MESSAGE("OBD", "UNABLE TO CONNECT", (millis() - startTime), cmd);
#endif
        return false;
      }
      //OK
      else if (strstr(content.c_str(), CHECK_OK_OBD) != NULL)
      {
#ifdef OBD_DEBUG
		LOG_MESSAGE("OBD", "OBDUART FOUND", (millis() - startTime), cmd);
#endif
        return true;
      }
      //Engine off? -> Leds Off
      else if (strstr(content.c_str(), CHECK_ERROR_ENGINEOUT) != NULL)
      {
#ifdef OBD_DEBUG
	LOG_MESSAGE("OBD", "ENGINE OFF", (millis() - startTime), cmd);
#endif
        buildLeds(0);
        return true;
      }
      //Rpm detected?
      else if (strstr(content.c_str(), CHECK_OK_RPMPREFIX) != NULL)
      {
#ifdef OBD_DEBUG
		  LOG_MESSAGE("OBD", "RPM FOUND", (millis() - startTime), cmd);
#endif
        //cut response (2 byte, 1 cmd 1 data - hexa)
        content.replace(CHECK_OK_RPMPREFIX, "");
        long val = convertRPM(content.c_str()) / 4;
#ifdef OBD_DEBUG
		LOG_MESSAGE("OBD", "Calculated RPM: ", val);
#endif
        //Add data to the queue so other thread can handle it
        if (xQueueSendToBack(obdDataQueue, &val, 20 / portTICK_RATE_MS) != pdTRUE)
        {
#ifdef OBD_DEBUG
		  LOG_MESSAGE("OBD", "Failed to queue rpm data");
#endif
        }
        return true;
      }
      //Businit error? -> Show error leds
      else if (strstr(content.c_str(), CHECK_ERROR_BUSINIT) != NULL)
      {

        leds[0] = CRGB::Black;
        leds[1] = INIT_COLOR;
        leds[2] = INIT_COLOR;
        leds[3] = INIT_COLOR;
        leds[4] = INIT_COLOR;
        leds[5] = INIT_COLOR;
        leds[6] = INIT_COLOR;
        leds[7] = INIT_COLOR;
        leds[8] = CRGB::Black;
        xSemaphoreTake(colorChange, portMAX_DELAY);
        FastLED.show();
        xSemaphoreGive(colorChange);
#ifdef OBD_DEBUG
		LOG_MESSAGE("OBD", "BUSINIT ERROR", (millis() - startTime), cmd);
#endif
        return false;
      }
    }
    else
    {
      //Timeout
      if (millis() - startTime >= OBD_TIMEOUT_MILLIS)
      {
#ifdef OBD_DEBUG
	LOG_MESSAGE("OBD","TIMEOUT",cmd);
#endif
        return false;
      }
    }
  }
}

/*
  Convert given rpm from hex to dec
*/
unsigned long convertRPM(const char *a)
{
  return hex2int(a, 4);
}

/*
  Convert given sequenze from hex to dec
*/
unsigned long hex2int(const char *a, unsigned int len)
{
  int i;
  unsigned long val = 0;
  for (i = 0; i < len; i++)
  {
    if (a[i] <= 57)
    {
      val += (a[i] - 48) * (1 << (4 * (len - 1 - i)));
    }
    else
    {
      val += (a[i] - 55) * (1 << (4 * (len - 1 - i)));
    }
  }
  return val;
}
/********************************************************************************************************/

/********************************************************************************************************/
/********************************************************************************************************/
//Led Processing Thread and Functions

void ledThread(void *pvParameter)
{

#ifdef SYSTEM_DEBUG
	LOG_MESSAGE("SYSTEM", "LED Processing Thread started on core ", xPortGetCoreID());
#endif

  long data;
  while (true)
  {
    //Wait for new data in queue
    if (xQueueReceive(obdDataQueue, &data, 60000 / portTICK_RATE_MS) != pdTRUE)
    { // max wait 60s
#ifdef LED_DEBUG
		LOG_MESSAGE("LED", "No Data in queue?!");
#endif
    }
    else
    {
      //Show rpm depending lights
      //Rpm matching is done here, in function buildLeds(int i) the amount and color of leds will be defined
      if ((data < 2100))
      {
        buildLeds(0);
      }
      else if ((data >= 2100) && (data < 2600))
      {
        buildLeds(1);
      }
      else if ((data >= 2600) && (data < 3050))
      {
        buildLeds(2);
      }
      else if ((data >= 3050) && (data < 3450))
      {
        buildLeds(3);
      }
      else if ((data >= 3450) && (data < 3800))
      {
        buildLeds(4);
      }
      else if ((data >= 3800) && (data < 4100))
      {
        buildLeds(5);
      }
      else if ((data >= 4100) && (data < 4400))
      {
        buildLeds(6);
      }
      else if ((data >= 4400) && (data < 4650))
      {
        buildLeds(7);
      }
      else if ((data >= 4650) && (data < 4900))
      {
        buildLeds(8);
      }
      else if ((data >= 4900))
      {
        buildLeds(9);
      }
      else
      {
        buildLeds(0);
      }
    }
  }
}

/*
  Define how much leds and which color should be displayed. Various modes can be defined.
*/
//Helper for flickering light
bool ledSwitchMode = true;
void buildLeds(int rpmLeds)
{
  if (shiftLightMode == 0)
  {
    if (rpmLeds == 0)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 1)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 2)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 3)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Green;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 4)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Green;
      leds[6] = CRGB::Green;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 5)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Green;
      leds[6] = CRGB::Green;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 6)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Yellow;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Green;
      leds[6] = CRGB::Green;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 7)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Orange;
      leds[3] = CRGB::Yellow;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Green;
      leds[6] = CRGB::Green;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 8)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Orange;
      leds[2] = CRGB::Orange;
      leds[3] = CRGB::Yellow;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Green;
      leds[6] = CRGB::Green;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds >= 9)
    {
      if (ledSwitchMode)
      {
        leds[0] = CRGB::Red;
        leds[1] = CRGB::Red;
        leds[2] = CRGB::Red;
        leds[3] = CRGB::Red;
        leds[4] = CRGB::Red;
        leds[5] = CRGB::Red;
        leds[6] = CRGB::Red;
        leds[7] = CRGB::Red;
        leds[8] = CRGB::Red;
        ledSwitchMode = false;
      }
      else
      {
        leds[0] = CRGB::Black;
        leds[1] = CRGB::Black;
        leds[2] = CRGB::Black;
        leds[3] = CRGB::Black;
        leds[4] = CRGB::Black;
        leds[5] = CRGB::Black;
        leds[6] = CRGB::Black;
        leds[7] = CRGB::Black;
        leds[8] = CRGB::Black;
        ledSwitchMode = true;
      }
    }
  }
  else if (shiftLightMode == 1)
  {
    if (rpmLeds == 0)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 1)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 2)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 3)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Green;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 4)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Green;
      leds[3] = CRGB::Green;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 5)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Green;
      leds[3] = CRGB::Green;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 6)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Green;
      leds[3] = CRGB::Green;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Yellow;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 7)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Green;
      leds[3] = CRGB::Green;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Yellow;
      leds[6] = CRGB::Orange;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 8)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Green;
      leds[3] = CRGB::Green;
      leds[4] = CRGB::Yellow;
      leds[5] = CRGB::Yellow;
      leds[6] = CRGB::Orange;
      leds[7] = CRGB::Orange;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds >= 9)
    {
      if (ledSwitchMode)
      {
        leds[0] = CRGB::Red;
        leds[1] = CRGB::Red;
        leds[2] = CRGB::Red;
        leds[3] = CRGB::Red;
        leds[4] = CRGB::Red;
        leds[5] = CRGB::Red;
        leds[6] = CRGB::Red;
        leds[7] = CRGB::Red;
        leds[8] = CRGB::Red;
        ledSwitchMode = false;
      }
      else
      {
        leds[0] = CRGB::Black;
        leds[1] = CRGB::Black;
        leds[2] = CRGB::Black;
        leds[3] = CRGB::Black;
        leds[4] = CRGB::Black;
        leds[5] = CRGB::Black;
        leds[6] = CRGB::Black;
        leds[7] = CRGB::Black;
        leds[8] = CRGB::Black;
        ledSwitchMode = true;
      }
    }
  }
  else if (shiftLightMode == 2)
  {
    if (rpmLeds == 0)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 1)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 2)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 3)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else if (rpmLeds == 4)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 5)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 6)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Yellow;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Yellow;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 7)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Yellow;
      leds[3] = CRGB::Orange;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Orange;
      leds[6] = CRGB::Yellow;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds == 8)
    {
      leds[0] = CRGB::Green;
      leds[1] = CRGB::Green;
      leds[2] = CRGB::Yellow;
      leds[3] = CRGB::Orange;
      leds[4] = CRGB::Red;
      leds[5] = CRGB::Orange;
      leds[6] = CRGB::Yellow;
      leds[7] = CRGB::Green;
      leds[8] = CRGB::Green;
    }
    else if (rpmLeds >= 9)
    {
      if (ledSwitchMode)
      {
        leds[0] = CRGB::Red;
        leds[1] = CRGB::Red;
        leds[2] = CRGB::Red;
        leds[3] = CRGB::Red;
        leds[4] = CRGB::Red;
        leds[5] = CRGB::Red;
        leds[6] = CRGB::Red;
        leds[7] = CRGB::Red;
        leds[8] = CRGB::Red;
        ledSwitchMode = false;
      }
      else
      {
        leds[0] = CRGB::Black;
        leds[1] = CRGB::Black;
        leds[2] = CRGB::Black;
        leds[3] = CRGB::Black;
        leds[4] = CRGB::Black;
        leds[5] = CRGB::Black;
        leds[6] = CRGB::Black;
        leds[7] = CRGB::Black;
        leds[8] = CRGB::Black;
        ledSwitchMode = true;
      }
    }
  }
  else if (shiftLightMode == -1)
  {
    leds[0] = CRGB::Black;
    leds[1] = CRGB::Black;
    leds[2] = CRGB::Black;
    leds[3] = CRGB::Black;
    leds[4] = CRGB::Black;
    leds[5] = CRGB::Black;
    leds[6] = CRGB::Black;
    leds[7] = CRGB::Black;
    leds[8] = CRGB::Black;
  }
  else
  {
    if (rpmLeds >= 9)
    {
      if (ledSwitchMode == 0)
      {
        ledSwitchMode = 1;
      }
      else
      {
        rpmLeds = 0;
        ledSwitchMode = 0;
      }
    }

    if (rpmLeds == 0)
    {
      leds[0] = CRGB::Black;
      leds[1] = CRGB::Black;
      leds[2] = CRGB::Black;
      leds[3] = CRGB::Black;
      leds[4] = CRGB::Black;
      leds[5] = CRGB::Black;
      leds[6] = CRGB::Black;
      leds[7] = CRGB::Black;
      leds[8] = CRGB::Black;
    }
    else
    {
      if (rpmLeds >= 1)
      {
        leds[8] = CRGB::Green;
      }
      else
      {
        leds[8] = CRGB::Black;
      }
      if (rpmLeds >= 2)
      {
        leds[7] = CRGB::Green;
      }
      else
      {
        leds[7] = CRGB::Black;
      }
      if (rpmLeds >= 3)
      {
        leds[6] = CRGB::Green;
      }
      else
      {
        leds[6] = CRGB::Black;
      }
      if (rpmLeds >= 4)
      {
        leds[5] = CRGB::Green;
      }
      else
      {
        leds[5] = CRGB::Black;
      }
      if (rpmLeds >= 5)
      {
        leds[4] = CRGB::Yellow;
      }
      else
      {
        leds[4] = CRGB::Black;
      }
      if (rpmLeds >= 6)
      {
        leds[3] = CRGB::Yellow;
      }
      else
      {
        leds[3] = CRGB::Black;
      }
      if (rpmLeds >= 7)
      {
        leds[2] = CRGB::Yellow;
      }
      else
      {
        leds[2] = CRGB::Black;
      }
      if (rpmLeds >= 8)
      {
        leds[1] = CRGB::Red;
      }
      else
      {
        leds[1] = CRGB::Black;
      }
      if (rpmLeds >= 9)
      {
        leds[0] = CRGB::Red;
        leds[1] = CRGB::Red;
        leds[2] = CRGB::Red;
        leds[3] = CRGB::Red;
        leds[4] = CRGB::Red;
        leds[5] = CRGB::Red;
        leds[6] = CRGB::Red;
        leds[7] = CRGB::Red;
        leds[8] = CRGB::Red;
      }
      else
      {
        leds[0] = CRGB::Black;
      }
    }
  }
  //Show leds
  xSemaphoreTake(colorChange, portMAX_DELAY);
  vTaskDelay(5 / portTICK_PERIOD_MS);
  FastLED.show();
  xSemaphoreGive(colorChange);
}
/********************************************************************************************************/

/********************************************************************************************************/
/********************************************************************************************************/
//Lightsensor Thread
void lightsensorThread(void *pvParameter)
{
#ifdef SYSTEM_DEBUG
	LOG_MESSAGE("SYSTEM", "Light Sensor Processing Thread started on core", xPortGetCoreID());
#endif
  //Check if the light changed, and how much it changed. if, over a defined time (DIM_TIME_MILLIS)
  //the light was constantly higher, the lights will change their brightness.
  unsigned long lastUpdateTime = millis();
  unsigned long lastBigChangeTime = 0;
  int lastReading = 255;
  int currentReading = 0;
  bool brightnessChangeDone = true;

  while (true)
  {
    //Read from sensor
    currentReading = analogRead(LIGHTSENSOR_PIN);// / LIGHTSENSOR_DIVIDER;
#ifdef BRIGHTNESS_DEBUG
	LOG_MESSAGE("BRIGHTNESS", "Current LED reading:",currentReading);//+currentReading);
#endif
#ifdef MODE_SWITCH_BUTTON
    bool btnPressed = checkButtonPressed(currentReading);
    if(btnPressed){
      
    }
#endif
    //Map reading to defined min and max brightness
    currentReading = map(currentReading, 0, 4096, MIN_LED_BRIGHTNESS, MAX_LED_BRIGHTNESS);
	currentReading *= 4;
	if (currentReading > 64) {
		currentReading = 64;
	}

#ifdef BRIGHTNESS_DEBUG
	LOG_MESSAGE("BRIGHTNESS","Calculated LED Brightness:",currentReading);
#endif

    int difference = currentReading - lastReading;

    if (abs(difference) >= 2)
    { //Big Change detected
#ifdef BRIGHTNESS_DEBUG
		LOG_MESSAGE("BRIGHTNESS", "Big brightness change detected!");
#endif
      lastBigChangeTime = millis();
      brightnessChangeDone = false;
    }
    else
    {
      if (!brightnessChangeDone)
      {
        //Brightness changed and no adjustment of the leds was done until now
        if ((millis() - lastBigChangeTime) > DIM_TIME_MILLIS)
        {
#ifdef BRIGHTNESS_DEBUG
		  LOG_MESSAGE("BRIGHTNESS","Changing lights!");
#endif 
    //change brightness of leds
          fadeLedsToBrightness(currentReading);
          brightnessChangeDone = true;
        }
      }
    }

    lastReading = currentReading;
#ifdef BRIGHTNESS_DEBUG
	LOG_MESSAGE("BRIGHTNESS", "Current LED Brightness:", FastLED.getBrightness());
#endif
    vTaskDelay(50 / portTICK_PERIOD_MS);
  }
}

/*
  Smooth change of led brightness
*/
void fadeLedsToBrightness(int targetBrightness)
{
  xSemaphoreTake(colorChange, portMAX_DELAY);
  if (FastLED.getBrightness() > targetBrightness)
  {
    while (FastLED.getBrightness() > targetBrightness)
    {
      FastLED.setBrightness(FastLED.getBrightness() - 1);
      vTaskDelay(2 / portTICK_PERIOD_MS);
      FastLED.show();
    }
  }
  else
  {
    while (FastLED.getBrightness() < targetBrightness)
    {
      FastLED.setBrightness(FastLED.getBrightness() + 1);
      vTaskDelay(2 / portTICK_PERIOD_MS);
      FastLED.show();
    }
  }
  xSemaphoreGive(colorChange);
}
bool buttonReleased = true;
bool checkButtonPressed(int currentReading)
{
  if (currentReading >= 4095 && buttonReleased)
  {
    if (shiftLightMode == 0)
    {
      shiftLightMode = 1;
      setShiftLightMode(1);
    }
    else if (shiftLightMode == 1)
    {
      shiftLightMode = 2;
      setShiftLightMode(2);
    }
    else if (shiftLightMode == 2)
    {
      shiftLightMode = 0;
      setShiftLightMode(0);
    }
    buttonReleased = false;
    return true;
  }
else if (currentReading >= 4095 && !buttonReleased){
  return false;
}
  else {
    buttonReleased = true;
    return false;
  }
}
/********************************************************************************************************/

/********************************************************************************************************/
/********************************************************************************************************/

BLECharacteristic *pCharacteristic;
String bluetoothTxValue = VERSION;
//BLUETOOTH Thread
//Callback for connecting and disconnecting
class MyServerCallbacks : public BLEServerCallbacks
{
  bool deviceConnected = false;

  void onConnect(BLEServer *pServer)
  {
    deviceConnected = true;
  };

  void onDisconnect(BLEServer *pServer)
  {
    deviceConnected = false;
  }
};

//Callback for bluetooth interaction
class MyCallbacks : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string rxValue = pCharacteristic->getValue();
    String rxData = rxValue.c_str();
#ifdef BLUETOOTH_DEBUG
	LOG_MESSAGE("BLUETOOTH", "Data received:", rxValue.c_str());
#endif
    if (rxData.equals("setMode0"))
    {
      shiftLightMode = 0;
      setShiftLightMode(0);
    }
    else if (rxData.equals("setMode1"))
    {
      shiftLightMode = 1;
      setShiftLightMode(1);
    }
    else if (rxData.equals("setMode2"))
    {
      shiftLightMode = 2;
      setShiftLightMode(2);
    }
    else if (rxData.equals("setModeOff"))
    {
      shiftLightMode = -1;
      setShiftLightMode(-1);
    }
  };
};

void bluetoothThread(void *pvParameter)
{
#ifdef SYSTEM_DEBUG
	LOG_MESSAGE("SYSTEM", "Bluetooth Thread started on core", xPortGetCoreID());
#endif
  //Set name according to firmware version
  BLEDevice::init(VERSION);

  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  BLEDescriptor rpmDescriptor(DESCRIPTOR_UUID);
  BLEDescriptor bootCntDescriptor(DESCRIPTOR_UUID);
  BLEDescriptor lightModeDescriptor(DESCRIPTOR_UUID);
  BLEDescriptor cmdLightModeDescriptor(DESCRIPTOR_UUID);
  BLEDescriptor consoleDescriptor(DESCRIPTOR_UUID);

  rpmDescriptor.setValue("RPM in R/Min");
  bootCntDescriptor.setValue("Boot Counter");
  lightModeDescriptor.setValue("Current Light Mode");
  cmdLightModeDescriptor.setValue("Light Mode Controle");
  cmdLightModeDescriptor.setValue("Debug Console");

  BLECharacteristic bootCntCharacteristic(BOOT_CNT_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
  BLECharacteristic lightModeCharacteristic(LIGHT_MODE_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
  BLECharacteristic cmdLightModeCharacteristic(CMD_LIGHT_MODE_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_WRITE);
  BLECharacteristic consoleCharacteristic(CONSOLE_CHARACTERISTIC_UUID, BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);

  bootCntCharacteristic.addDescriptor(&bootCntDescriptor);
  lightModeCharacteristic.addDescriptor(&lightModeDescriptor);
  cmdLightModeCharacteristic.addDescriptor(&cmdLightModeDescriptor);
  consoleCharacteristic.addDescriptor(&consoleDescriptor);

  cmdLightModeCharacteristic.setCallbacks(new MyCallbacks());

  pService->addCharacteristic(&bootCntCharacteristic);
  pService->addCharacteristic(&lightModeCharacteristic);
  pService->addCharacteristic(&cmdLightModeCharacteristic);
  pService->addCharacteristic(&consoleCharacteristic);

  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();

#ifdef BLUETOOTH_DEBUG
  LOG_MESSAGE("BLUETOOTH", "BLUETOOTH ready and initialized");
#endif

  bootCntCharacteristic.setValue("" + bootCounter);
  bootCntCharacteristic.notify();

  lightModeCharacteristic.setValue("" + shiftLightMode);
  lightModeCharacteristic.notify();

  int lightModeChangeData;
  while (true)
  {
    //Update characteristic if mode changed
    if (xQueueReceive(lightModeChangeQueue, &lightModeChangeData, 100 / portTICK_RATE_MS) != pdTRUE)
    { // max wait 60s
#ifdef BLUETOOTH_DEBUG
		LOG_MESSAGE("BLUETOOTH", "No Data in light mode queue?!");
#endif
    }
    else
    {
      lightModeCharacteristic.setValue("" + lightModeChangeData);
      lightModeCharacteristic.notify();
#ifdef BLUETOOTH_DEBUG
	  LOG_MESSAGE("BLUETOOTH", "Updated light mode characteristic");
#endif
    }

    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}
/********************************************************************************************************/


/********************************************************************************************************/
/********************************************************************************************************/
//Lightsensor Thread
void wifidebugThread(void *pvParameter)
{
#ifdef SYSTEM_DEBUG
	LOG_MESSAGE("SYSTEM", "WiFi Debug Thread started on core", xPortGetCoreID());
#endif

	const char* ssid = "ShiftIndicator";
	const char* password = "123789654";
	WiFiServer wifiServer(80);
	WiFi.softAP(ssid, password);
	char data[200];

	wifiServer.begin();

	while (true)
	{
		WiFiClient client = wifiServer.available();

		if (client) {
			LOG_MESSAGE("WIFI_DEBUG", "Client connected");
			while (client.connected()) {

				if (xQueueReceive(wifiDebugQueue, &data, 60000 / portTICK_RATE_MS) != pdTRUE)
				{ // max wait 60s
#ifdef WIFI_DEBUG
					LOG_MESSAGE("WIFI_DEBUG", "No Data in queue?!");
#endif
				}
				else
				{
					client.write(data);
					client.write("\r\n");
				}
				vTaskDelay(50 / portTICK_PERIOD_MS);
			}

			client.stop();
#ifdef WIFI_DEBUG
			LOG_MESSAGE("WIFI_DEBUG", "Client disconnected");
#endif
			vTaskDelay(500 / portTICK_PERIOD_MS);
		}
	}
}
/********************************************************************************************************/

//MAIN
void setup()
{
  //Begin serials
  DebugSerial.begin(38400);
  ObdSerial.begin(38400);

#ifdef SYSTEM_DEBUG
  LOG_MESSAGE("SYSTEM", VERSION);
#endif
  //Create semaphores for led dimming and light mode change
  colorChange = xSemaphoreCreateMutex();
  shiftLighModeChange = xSemaphoreCreateMutex();

  //Intialize leds
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(MAX_LED_BRIGHTNESS);

  //Initialize esp32 preferences
  preferences.begin("rpm", false);
  //Load current bootcounter and increase it
  bootCounter = loadBootCount();
  bootCounter++;
  //Save it to esp32
  setBootCount(bootCounter);

  //Load shift light mode from esp32
  shiftLightMode = loadShiftLightMode();
#ifdef PREFERENCES_DEBUG
  LOG_MESSAGE("PREFERENCES", "Preferences ready. Bootcount:",bootCounter);
#endif // PREFERENCES_DEBUG

  //Create queue for interthread data exchange
  obdDataQueue = xQueueCreate(10, sizeof(long));
  lightModeChangeQueue = xQueueCreate(10, sizeof(int));
  wifiDebugQueue = xQueueCreate(20, sizeof(char[200]));

  //Create threads
  xTaskCreate(ledThread, "LED", 4096, NULL, NORMAL_PRIO, NULL);
  xTaskCreate(obdThread, "OBD", 4096, NULL, HIGH_PRIO, NULL);
  xTaskCreate(lightsensorThread, "LIGHT", 4096, NULL, LOW_PRIO, NULL);

#ifdef WIFI
  xTaskCreate(wifidebugThread, "WIFIDEBUG", 4096, NULL, LOW_PRIO, NULL);
#endif

#ifdef BLUETOOTH
  xTaskCreate(bluetoothThread, "BLUETOOTH", 4096, NULL, LOW_PRIO, NULL);
#endif

#ifdef SYSTEM_DEBUG
  LOG_MESSAGE("SYSTEM", "Main Setup done!");
#endif

}

unsigned int loadBootCount()
{
  return preferences.getUInt("bootCounter", 0);
}

void setBootCount(unsigned int tmpBootCount)
{
  int res = preferences.putUInt("bootCounter", tmpBootCount);
}

int loadShiftLightMode()
{
  xSemaphoreTake(colorChange, portMAX_DELAY); //Threadsafety
  int tmp = preferences.getInt("shiftLightMode", 0);
  xSemaphoreGive(colorChange);
  return tmp;
}

void setShiftLightMode(int tmpShiftLightMode)
{
  xSemaphoreTake(colorChange, portMAX_DELAY); //Threadsafety
  shiftLightMode = tmpShiftLightMode;
  int res = preferences.putInt("shiftLightMode", tmpShiftLightMode);

  if (xQueueSendToBack(obdDataQueue, &shiftLightMode, 20 / portTICK_RATE_MS) != pdTRUE)
  {
#ifdef PREFERENCES_DEBUG
	  LOG_MESSAGE("PREFERENCES","Failed to queue light mode data");
#endif
  }
  xSemaphoreGive(colorChange);
}

void LOG_MESSAGE(char* type, char* message) {
	char buffer[200];
	sprintf(buffer, "[%s][%s] - %s", String(millis()).c_str(), type, message);

#ifdef SERIAL_DEBUG
	DebugSerial.println(buffer);
#endif

#ifdef WIFI
	if (xQueueSendToBack(wifiDebugQueue, &buffer, 20 / portTICK_RATE_MS) != pdTRUE)
	{
#ifdef PREFERENCES_DEBUG
		LOG_MESSAGE("PREFERENCES", "Failed to queue wifi debug data");
#endif
}
#endif
}

void LOG_MESSAGE(char* type, char* message, int value) {
	char buffer[200];
	sprintf(buffer, "[%s][%s] - %s %i ", String(millis()).c_str(), type, message, value);

#ifdef SERIAL_DEBUG
	DebugSerial.println(buffer);
#endif

#ifdef WIFI
	if (xQueueSendToBack(wifiDebugQueue, &buffer, 20 / portTICK_RATE_MS) != pdTRUE)
	{
#ifdef PREFERENCES_DEBUG
		LOG_MESSAGE("PREFERENCES", "Failed to queue wifi debug data");
#endif
}
#endif
}

void LOG_MESSAGE(char* type, char* message, const char* value) {
	char buffer[200];
	sprintf(buffer, "[%s][%s] - %s %s ", String(millis()).c_str(), type, message, value);

#ifdef SERIAL_DEBUG
	DebugSerial.println(buffer);
#endif

#ifdef WIFI
	if (xQueueSendToBack(wifiDebugQueue, &buffer, 20 / portTICK_RATE_MS) != pdTRUE)
	{
#ifdef PREFERENCES_DEBUG
		LOG_MESSAGE("PREFERENCES", "Failed to queue wifi debug data");
#endif
}
#endif
}

void LOG_MESSAGE(char* type, char* result, unsigned long time, const char* command) {
	char buffer[200];
	sprintf(buffer, "[%s][%s] - %s - %lu - %s", String(millis()).c_str(), type, result, time,command);

#ifdef SERIAL_DEBUG
	DebugSerial.println(buffer);
#endif

#ifdef WIFI
	if (xQueueSendToBack(wifiDebugQueue, &buffer, 20 / portTICK_RATE_MS) != pdTRUE)
	{
#ifdef PREFERENCES_DEBUG
		LOG_MESSAGE("PREFERENCES", "Failed to queue wifi debug data");
#endif
	}
#endif
}

void loop() {}
